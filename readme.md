# **Entenda de uma vez o que é README**

## 1. Entendendo o *.md*
###  1.1 **Markdown**

* Markdown ou simplesmente ".md" é uma linguagem de marcação no qual serve para organizar o seu texto, facilitando a leitura. 
* Imagine o trabalho que seria ler um manual de montagens de um equipamento no bloco de notas. Você poderia até montar o equipamento, mas demoraria muito! 

---
### 1.2 **README**

* O Readme(Leia-me, em português), como o nome já sugere, é um documento que implora para ser lido, porem muitos(as) desenvolvedores(as) esquecem de ler antes de clonar o projeto de outra pessoa. 
* Nele pode conter informações importantes do projeto, como instalar, bibliotecas necessárias, contribuidores e outras coisas úteis.
---

## 2. Principais marcadores

* Titulo e subtítulos;
* Paragrafos;
* Tabelas;
* Links;
* Listas ordenadas;
* Listas não ordenadas;

---

## Colaboradores:

| Nome | Instagram | Gitlab | 
|---   | ---   |---     |
|Matheus Oliveira | @devmatheusoliveira | @matheusoliveira99 |
